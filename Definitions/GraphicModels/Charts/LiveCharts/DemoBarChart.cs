﻿using LiveCharts;
using LiveCharts.Uwp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Definitions.GraphicModels.Charts
{
    public class DemoBarChart
    {
        public SeriesCollection SeriesCollection { get; set; }

        public string[] Labels { get; set; }

        public Func<double, string> Formatter { get; set; }

        public DemoBarChart(int _limit = 100)
        {
            SeriesCollection = new SeriesCollection
            {
                new LiveCharts.Uwp.ColumnSeries
                {
                    Title = "Battery Cells",
                    Values = GenerateRandoms(_limit)
                }
            };

            SetDefaultLabels();
            Formatter = value => value.ToString("N");

        }

        private ChartValues<double> GenerateRandoms(int _limit)
        {
            columnLimit = _limit;
            ChartValues<double> randData = new ChartValues<double>();
            Random rnd = new Random();
            for(int i = 0; i < _limit; i++)
                randData.Add(rnd.Next(0,101)*1.0d);
            return randData;
        }

        private void SetDefaultLabels()
        {
            List<string> _labels = new List<string>();
            for (int i = 0; i < columnLimit; i++)
                _labels.Add($"{i + 1} Cell");
            Labels = _labels.ToArray();
        }

        private int columnLimit;
    }
}
