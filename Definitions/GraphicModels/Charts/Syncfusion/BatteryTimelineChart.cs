﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Definitions.GraphicModels.Charts
{
    public class BatteryTimelineChart
    {
        public Tuple<DateTime, float> Data { get; set; }

        public ConcurrentQueue<Tuple<DateTime, float>> Queue { get; set; }

        public IEnumerable<DateTime> TimeInterval { get; set; }
        public IEnumerable<float> DataInternval { get; set; }

    }
}
