﻿using Data_Visualization_Tool.Definitions.Vehicle;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Definitions.GraphicModels.Charts
{
    public class BatteryTemperatureChart
    {
        public ICollection<string> Labels { get; set; }
        public int NumberOfCells { get; set; }
        public ObservableCollection<BatteryCell> Cells { get; set; }

        public BatteryTemperatureChart(bool _isDemo = false, int _numberOfCells = 0)
        {
            if(_isDemo)
            {
                Cells = Generate100(_numberOfCells);
                Labels = AddLabels();
            }
        }

        private ICollection<string> AddLabels()
        {
            ICollection<string> _Labels = new List<string>();
            foreach(BatteryCell cell in Cells)
            {
                _Labels.Add(cell.Label);
            }
            return _Labels;
        }

        private ObservableCollection<BatteryCell> Generate100(int _numberOfCells)
        {
            ObservableCollection<BatteryCell> cells = new ObservableCollection<BatteryCell>();
            for(int i = 0; i < _numberOfCells; i++)
            {
                cells.Add(new BatteryCell(i + 1, 100));
            }
            return cells;
        }
    }
}
