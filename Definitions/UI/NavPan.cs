﻿using Data_Visualization_Tool.Definitions.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Definitions.UI
{
    public class NavPan
    {
        public IEnumerable<Option> Options { get; }

        public NavPan(IEnumerable<Option> _options)
        {
            this.Options = _options;
        } 

        public string OptionsToString
        {
            get { return string.Join(", ", Options.Select(x => x.Name)); }
        }

    }
}
