﻿using Data_Visualization_Tool.Definitions.Entities;
using Data_Visualization_Tool.Definitions.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Definitions.Pages
{
    public class MainPageModel
    {
        public NavPan NavigationBar { get; set; }

        private List<Option> Options;

        public MainPageModel()
        {
            Options = new List<Option>();

            for (int x = 0; x < 100; x++)
                Options.Add(new Option($"{x + 1} Cell Battery"));
        }
    }
}
