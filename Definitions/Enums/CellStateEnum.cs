﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Visualization_Tool.Definitions.Enums
{
    public enum CellStateEnum
    {
        Discharged,
        FullyCharged,
        Charging,
        Discharging,
        Idle,
    }
}
