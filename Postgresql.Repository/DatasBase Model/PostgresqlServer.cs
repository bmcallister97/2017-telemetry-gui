﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Repository.DatasBase_Model
{
    [DataContract(Name = "WFE Postgresql Server")]
    public class PostgresqlServer
    {
        [DataMember]
        public string hostServer { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        private string password { get; set; }
        [DataMember]
        public string database { get; set; }

        public void ChangePassword(string _pwd)
        {
            password = _pwd;
        }

        [DataMember]
        public string ConnectionString { get {
                return $"Host={hostServer};Username={username};Password={password};Database={database}";
            }  }

    }
}
