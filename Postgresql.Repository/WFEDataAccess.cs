﻿using Npgsql;
using Repository.DatasBase_Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Postgresql.Repository
{
    [DataContract]
    public class WFEDataAccess : IWFEDataAccess
    {
        [DataMember]
        private PostgresqlServer Server { get; set; }
        [DataMember]
        private NpgsqlConnection connection { get; set; }

        public WFEDataAccess()
        {
            Server = new PostgresqlServer();
        }

        public void ConnectToPostgresql()
        {
            if(connection == null)
            {
                connection = new NpgsqlConnection(Server.ConnectionString);
            }
            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                
            }
        }

        public void DisconnectPostgesql()
        {
            if(connection != null)
            {
                try
                {
                    connection.Close();
                }
                catch(Npgsql.NpgsqlException ne)
                {
                    
                }
            }
            
        }

        public Tuple<int, string> GetBatteryCellTemperature()
        {
            throw new NotImplementedException();
        }
    }
}
