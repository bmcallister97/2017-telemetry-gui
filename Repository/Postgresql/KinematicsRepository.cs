﻿using Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Postgresql
{
    public class KinematicsRepository : IKinematicsRepository
    {
        public Task<double> UpdateAcceleration()
        {
            throw new NotImplementedException();
        }

        public Task<double> UpdateBatteryLevel()
        {
            throw new NotImplementedException();
        }

        public Task UpdateCANTraffic()
        {
            throw new NotImplementedException();
        }

        public Task UpdateDirection()
        {
            throw new NotImplementedException();
        }

        public Task UpdateGPSLocation()
        {
            throw new NotImplementedException();
        }

        public Task<double> UpdatePower()
        {
            throw new NotImplementedException();
        }

        public Task<double> UpdateSpeed()
        {
            throw new NotImplementedException();
        }
    }
}
