﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Enum
{
    public enum DBConnectionStatus
    {
        Failed,
        Idle,
        Connected,
        Exception,
    }

    public enum DBTransactionStatus
    {
        Successful,
        Failed,
    }

}
