﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.IRepository
{
    public interface IKinematicsRepository
    {
        Task<double> UpdateSpeed();

        Task<double> UpdateBatteryLevel();

        Task UpdateGPSLocation();

        Task<double> UpdateAcceleration();

        Task UpdateDirection();

        Task UpdateCANTraffic();

        Task<double> UpdatePower();

    }
}
